const http = require("http");
const os = require("os");
const  moment = require('moment');

const server = http.createServer(function(req,res){

    res.writeHead(200,{"Content-Type":"application/json"})
    let o = {hostname:os.hostname(),time:moment().format()};
    res.end(JSON.stringify(o));

});

server.listen(3000,function(){
    console.log("server has starting");

})