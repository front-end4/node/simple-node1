FROM node
MAINTAINER amgjum@qq.com
COPY . /app/
WORKDIR /app
RUN npm install
EXPOSE 3000
CMD ["node", "index.js"]